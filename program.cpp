#include <iostream>
#include "list.h"
#include "list.cpp"

using namespace std;
void main()
{
	//Sample Code

	List<char> mylist;

	mylist.pushToTail('k');
	mylist.pushToTail('e');
	mylist.pushToTail('n');
	mylist.popTail();//call function poptail
	mylist.print();
	cout << endl;
	char input;//create char value for search
	cout << "Input a character for Search : ";
	cin >> input;//recieve character
	if (mylist.search(input) == true) {//if call function is true display character found
		cout << "Character found!\n";
	}
	else {//if call function is false display character didn't found
		cout << "Character didn't found!\n";
	}

	//TO DO! Complete the functions, then write a program to test them.
	//Adapt your code so that your list can store data other than characters - how about a template?
	system("pause");
}